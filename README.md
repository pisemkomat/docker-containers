# Quizdo.eu (Písemkomat.cz)
App for simplify generating school exams using context (print, online, ...) aware generator.

## First run
1. Make sure you have installed Docker and Docker Compose (v2) on your machine and your user is member of the Docker group
1. Clone the root repository and submodules and update to the latest code  
`git clone --recursive git@gitlab.com:pisemkomat/docker-containers.git containers`  
`git submodule update --remote --recursive` from the root of the repository

1.  Prepare `.env` file:
**never commit `.env` file to git, keep it local!!!**
    - Go to `./containers/`, copy `.env_example` and rename it to `.env`
        - `cp .env_example .env`
    - Fill missing or incorrect values (ask for current .env file)

1. Export User ID and Group ID to your environment (by editing `.bashrc` or `.zshrc`):  
`export UID=$(id -u)`  
`export GID=$(id -g)`

> Now a script was prepared to do the rest of the preparations.
>
> **CD to `./containers/` and run `make`**. That's IT. Enjoy. ;)

### Script prepared everything. But if you'd like to replicate the steps manually:

1. Download packages in `./nextapp` using package manager (`npm install`,`yarn`...)

1. **Run** `make` in the `./containers/` directory. This will build and run all containers and creates its files in the way that **allows future use without the need of SUDO privileges!!**

1. Switch to different terminal window to run actions below

1. Generate and migrate Prisma and GORM database models. Run make commands, that does it for you:  
`make fe-db-push` respectively `make be-db-migrate`  
**Previous command must be running, run these commands in separate terminal**

1. Go to `http://localhost:3000/en/app` and login using Google account. This will create a new user and session in the database and let you use the app.
> `API testing:` Copy the Session token and SessionID to the Postman Env and use it to authenticate in API

### Troubleshooting
- If any problem with Docker, try to
    - Look at the `groups` command
        - If you're not in Docker group, run `sudo usermod -aG docker $USER`, logout user and login again
    - Delete `./containers/data` directory (`may need SUDO for this`)
    - Restart the Docker service
    - Follow the first run steps again
- Other problem
    - Try to Google or ChatGPT it first men :)
    - Ask @sjiamnocna (Valgrimmd on Discord)

## Development run
**Implementation instructions** are located inside `README.md` of specific repositories 
1. **Run DB container** in the background
1. Start your desired container

### Frontend
`make fe` in the `./containers/` directory or `make` in the case of `./containers/nextapp` directory runs the container

### Backend
`make be` in the `./containers/` or `make` in the case of `./containers/goapp` directory runs the container

API test cases are **contained in the Postman** account.

## Important notes
- When working use own branch and refer to issue or merge request
- Mark places that needs improvements with TODO comments  
> // TODO: ...
- Api request and response types are exported from backend PERIODICALLY
- To use the API, please see the Postman account examples
- BACKEND: Update Postman requests everytime change is made

## Donate
If you like this, please donate. Any donation is appreciated.

[![Donate via Paypal](https://raw.githubusercontent.com/stefan-niedermann/paypal-donate-button/master/paypal-donate-button.png "Donate via Paypal")](https://www.paypal.com/donate/?business=65SS8NS48FPFQ&no_recurring=0&item_name=Thanks+for+supporting+me+in+developlent&currency_code=CZK)