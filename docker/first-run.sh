#!/bin/bash

set -e  # Exit on error
set -u  # Treat unset variables as an error
set -o pipefail  # Fail if any command in a pipeline fails

# Define the path to the .env file
ENV_FILE=".env"
source $ENV_FILE

# Check if .env file exists
if [ ! -f "$ENV_FILE" ]; then
  echo "❌ Error: $ENV_FILE file is missing. Create it and add the necessary environment variables."
  exit 1
fi

# Ensure required environment variables are set based on the .env file
REQUIRED_VARS=("UID" "GID" "DB_USER" "DB_PASSWORD" "DB_DATABASE" "GOOGLE_CLIENT_ID" "GOOGLE_CLIENT_SECRET" "DB_DATABASE" "DB_ROOT_PASSWORD")
for var in "${REQUIRED_VARS[@]}"; do
  if [[ -z "${!var:-}" ]]; then
    echo "❌ Error: Missing $var in $ENV_FILE"
    exit 1
  fi
done

echo "✅ Loaded environment variables:"

if ! command -v docker &> /dev/null; then
  echo "❌ Docker is not installed. Please install it first."
  exit 1
fi

if ! command -v docker compose &> /dev/null; then
  echo "❌ Docker Compose is not installed. Please install it first."
  exit 1
fi
echo "✅ Checked Docker and Docker Compose commands..."

cd frontend-next
if command -v bun &> /dev/null; then
  echo "✅ Using Bun for package installation..."
  bun install
elif command -v npm &> /dev/null; then
  echo "✅ Using npm for package installation..."
  npm install
elif command -v yarn &> /dev/null; then
  echo "✅ Using Yarn for package installation..."
  yarn install
else
  echo "❌ No package manager found. Please install Bun, npm, or Yarn."
  exit 1
fi

echo "✅ Frontend dependencies installed..."
cd ..

# Remove any existing containers to get a clean results
docker container prune -f

# Build and run images in background
echo "🚀 Starting Docker Compose..."
docker compose up -d --build

# Check that all services are running properly
containers=$(docker ps -a --format "{{.ID}} {{.Status}}")

while true; do
    containers=$(docker ps -a --format "{{.ID}} {{.Status}}")

    if echo "$containers" | grep -qE "(Dead|Exited|Restarting)"; then
        echo "❌ One or more containers died due to:" ${containers}
        exit 1
    fi

    if echo "$containers" | grep -qv "Up"; then
        echo "🚀 Waiting for services to start..."
    else
        echo "✅ All containers are running!"
        break
    fi
    sleep 5
done

# Now that services are up, we can detach
echo "✅ Services are running! Starting migration in 3 sec";
sleep 3;

# Migrations
# First run Prisma migrations (it resets the database) using ROOT access to the DB
echo "🚀 Running Prisma push...";
docker compose exec -T nextapp npx prisma generate;\
docker compose exec -T nextapp env DATABASE_URL="mysql://root:${DB_ROOT_PASSWORD}@db:3306/${DB_DATABASE}" npx prisma db push;\

if [[ $? ]]; then
    echo "Prisma ready continuing";
else
    echo "Prisma failed";
    docker compose down;
    exit 1;
fi;

# Fix the permissions for GO cache
echo "🚀 Fixing GO cache permissions..."
sudo mkdir -p data/goapp/.cache
sudo chown -R $UID:$GID data/goapp && sudo chmod -R 764 data/goapp;

# Run GORM migrations
echo "🚀 Running GORM migrations...";
docker compose exec -T goapp go run ./cmd/migrate/main.go;

if [[ $? ]]; then
    echo "GORM ready continuing";
else
    echo "GORM failed";
    exit 1;
fi;

# Shut down the containers to allow the user to start only necessary services
docker compose down;

echo "🚀 Project is set up!";
echo "👉 Run 'make db' to start database and then use 'make' / 'make fe' / 'make be' to start the project.";