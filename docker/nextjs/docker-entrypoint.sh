#!/bin/sh

# Generate Prisma client
npx prisma generate

# Wait until the database is ready
until npx prisma migrate dev --name docker_entrypoint; do
  >&2 echo "MySQL is unavailable - sleeping"
  sleep 3
done

# Apply migrations to the database
npx prisma db push

# Run the application
exec "$@"
