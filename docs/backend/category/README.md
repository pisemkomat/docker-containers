# Category endpoints
- Basic endpoints for CRUD category management.
- Category is the core of the Quizcraft app, allowing users to categorize their tasks and load them into quizzes.
- **Hierarchical levels** (using *ParentID*):
    1. Subject (Math, Physics, ...)
    2. Field (Adding, Quadratic equations, Thermodynamics, ...)
    3. Custom user defined categories
- [category.model.go](https://gitlab.com/pisemkomat/goapp/-/blob/main/model/category.model.go), [category.go](https://gitlab.com/pisemkomat/goapp/-/blob/main/endpoints/eapp/category.go)

## `GET /Cat/` - Get categories by parameters
- Query params: [JSONGetCategoriesQueryParams](https://gitlab.com/pisemkomat/goapp/-/blob/main/model/category.model.go):
    - `ID` - IDs of the category to get divided by commas
    - `Depth` - Depth of the category tree (children) to load
    - `Locale` - Language of the category names
    - `Author` - ID of the author of the category
    - `Fulltext` - Fulltext search in category names
    - `Public` - Get only categories marked as public, ignore private ones
- Response: [JSONGetCategoriesResponse](https://gitlab.com/pisemkomat/goapp/-/blob/main/model/category.model.go) | [JSONGetMultipleFullCategoryResponse](https://gitlab.com/pisemkomat/goapp/-/blob/main/model/category.model.go) if using ID
- Errors (HTTP status code):
  - 400: Bad request (invalid format or combination of query param) - see query params above
  - 403: User session was not identified or user doesn't have permission to view given categories
  - 404: Not found - No categories found with given parameters
  - 417: Expectation failed - Error with loading category structure

## `POST /Cat/` - Create or update category if exists
- No query params, everything is included in body data
- Body data: [JSONUpdateCategoryRequest](https://gitlab.com/pisemkomat/goapp/-/blob/main/model/category.model.go)
- Response: [JSONUpdateCategoryResponse](https://gitlab.com/pisemkomat/goapp/-/blob/main/model/category.model.go)
- Errors (HTTP status code):
  - 400: Bad request (invalid data format or combination of fields) - see types above
  - 403: User session was not identified or user doesn't have permission to update or amend given category
  - 404: Not found - ID for update entered but not found in database
  - 409: Conflict - Category with the same data already exists
  - 417: Expectation failed - Either parent category doesn't exist or you are trying to create a category that's its own parent

## `DELETE /Cat/` - Delete category
- Will mark the category as deleted via GORM Delete method.
- Query params (see [JSONDeleteCategoryQueryParam](https://gitlab.com/pisemkomat/goapp/-/blob/main/model/category.model.go))
  - `ID` - ID of cat(s) divided by `,`
- Body data: None
- Response: [JSONDeleteCategoryResponse](https://gitlab.com/pisemkomat/goapp/-/blob/main/model/category.model.go)