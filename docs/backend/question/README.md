# Questions endpoints
- Questions contain raw question data that can be used by the generator to produce results
- Each question belongs to one category
- Questions can be tagged with multiple tags
- **Usage**: in exams and single rendered results
- [question.model.go](https://gitlab.com/pisemkomat/goapp/-/blob/main/model/question.model.go), [question.go](https://gitlab.com/pisemkomat/goapp/-/blob/main/endpoints/eapp/question.go)

## `GET /Question/` - Get raw question data identified by specified parameters
- Query params (see `JSONGetQuestionQueryParams`):
  - `ID` - Comma-separated list of question IDs to retrieve full question data
  - `Locale` - Comma-separated list of language codes, other options: (user|all)
    - user: load user's default locale(s)
    - all: load all available question locales
  - `Author` - Question author data
  - `Fulltext` - Fulltext search in question name, content and description
  - `Public` - Boolean flag to get questions with public visibility
  - `Cats` - Category ID(s) to sort questions by categories
  - `Tags` - Comma-separated list of tag IDs to filter questions
- Response: `JSONGetQuestionResponse`s
- Errors (HTTP status code):
  - `400`: Bad request - Invalid format or combination of query parameters
  - `403`: Forbidden - User not authenticated or lacks required permissions
  - `404`: Not found - No questions match the given parameters
  - `417`: Expectation failed - Error loading question structure

### `GET /Question/render` - Validates and render question, optionally saves the result and allow to share the result using hash
- Query params (`JSONRenderQuestionQueryParam`):
  - `hash` - Load generated question from database
  - `validate` - Responds only boolean value if data are correct (valid/invalid)
  - `dry` - One time render (validation etc.), wont store the result in database
- Response: `QuestionGenerated[]`:
- Errors (HTTP status code):
  - `400`: Bad request - Invalid data format
  - `417`: Expectation failed - Rendering engine error
  - `500`: Internal error - Failed to save render result (only when dry=false)

## `POST /Question/` - Create or update question
- Body data: `JSONUpdateQuestionRequest` containing:
  - `ID` - Question ID (optional, for updates)
  - `Title` - Question title
  - `Content` - Question content/parameters
  - `CategoryID` - ID of the category
  - `Tags` - Array of tag IDs
  - `Public` - Boolean flag for public visibility
  - `Locale` - Language code
- Response: `JSONQuestionResponse`
- Errors (HTTP status code):
  - `400`: Bad request - Invalid data format or field combination
  - `403`: Forbidden - User not authenticated or lacks required permissions
  - `404`: Not found - Question ID not found for update
  - `409`: Conflict - Duplicate question detected
  - `417`: Expectation failed - Invalid parent question or circular reference

## `DELETE /Question/` - Delete question(s)
- Soft deletes question using GORM Delete method
- Query params (`JSONDeleteQuestionQueryParam`):
  - `ID` - Comma-separated list of question IDs to delete
- Response: `JSONDeleteQuestionResponse` containing:
  - `DeletedCount` - Number of questions deleted
- Errors (HTTP status code):
  - `400`: Bad request - Invalid ID format
  - `403`: Forbidden - User not authenticated or lacks delete permissions
  - `404`: Not found - One or more question IDs not found