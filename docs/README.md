# App specification

## Description
> This application helps teachers and instructors to efficiently generate different versions of exams from tasks (questions) with parametrically defined properties.
>
> The main goal is to simplify and modernize the process of exam preparation and tailor it to the needs of modern education.

### Features:
- Simple generation of multiple versions (values, languages) based a parametrically specified tasks.
- Create exam from specified tasks and defined number of tasks from chosen categories, combining and randomizing order, using defined language.
- Supports both closed and open questions.
- Context aware exam generation - different outputs based on user needs;
   - Style of the question, answers, inputs (printed/online etc.)
   - Choose language of the tasks for the whole exam.
- Generating values and performing calculations with it
- Evaluating the task and the resulting exam qualities according to paedagogic theory using AI.
   - [Taxonomy of learning tasks according to D. Tollinger (1970)](https://is.muni.cz/el/1441/podzim2012/ZS1BK_PDD/um/Tollingerova.pdf)
- Integration with other systems using API.

## Containers
> Containers are used to automate many things in the development and production.
>
> Request to the server are proxied by Nginx to the container (**Frontend** or **API**)
>
> Also they're made to add part of security to the app
>
> We use `docker-compose.yml` and `docker-compose.prod.yml` for production
>
> To simplify working with it, it's defined in 

![alt text](./assets/containers_schema.png "App containers and used technologies schema")
1. **Frontend**
   - Single Page Application (SPA)
   - Technologies: React (NextJS), TypeScript, Tailwind
   - Functions:
        - Public website
        - User authentication (AuthJS)
        - User interface for managing and displaying examples, tests, campaigns (progressive mode)
        - Interactive student testing and collection of responses, teacher access to results
   - Communication with **Backend** through API, using NextJS server actions.
   
2. **Backend**
   - REST API
   - Technologies: Go, Gin, GORM
   - Function:
   Providing data for the frontend, generating data and rendering questions/tests, integration with the database.

3. **Database**
   - Relational Database Management System (RDBMS)
   - Technologies: MariaDB
   - Function: Storing categories, test questions, test templates, user data, and results.