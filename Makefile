mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

default:
	echo "Development version";\
	if [ ! -d "./data" ]; then\
		echo "This is the first run. Dont forget to setup .env variables.";\
		echo "Preparing project with script:";\
		./docker/first-run.sh;\
	else\
		echo "Project is already set up. Run up the DB (make db) and wanted container (make fe/be) separately.";\
	fi;
prod:
	docker compose -f docker-compose.prod.yml up -d --build
db:
	docker compose up -d --no-recreate --build db
fe:
	docker compose up --build --no-deps --no-recreate nextapp
fe-shell:
	container=$$(docker ps -f name="${current_dir}-nextapp" -q) &&\
	if [ -z "$$container" ]; then\
		echo "Container is not running, please start it first";\
	else\
		docker exec -it "$$container" sh;\
	fi;
fe-db-push:
	container=$$(docker ps -f name="${current_dir}-nextapp" -q) &&\
	if [ -z "$$container" ]; then\
		echo "Container is not running, please start it first";\
	else\
		docker exec -it "$$container" npx prisma generate;\
		docker exec -it "$$container" npx prisma db push;\
	fi;
be:
	docker compose up --build --no-deps --no-recreate goapp
be-shell:
	container=$$(docker ps -f name="${current_dir}-goapp" -q) &&\
	if [ -z "$$container" ]; then\
		echo "Container is not running, please start it first";\
	else\
		docker exec -it "$$container" sh;\
	fi;
be-db-migrate:
	container=$$(docker ps -f name="${current_dir}-goapp" -q) &&\
	if [ -z "$$container" ]; then\
		echo "Container is not running, please start it first";\
	else\
		docker exec -it "$$container" go run ./cmd/migrate/main.go;\
	fi;
reset:
# In case you need to remove everything and start clean
	docker compose down;\
	read -p "Really delete all data to allow (another) first-run (y/n)?" choice;\
	case "$$choice" in \
	y|Y ) echo "OK, Your call :)" && sudo rm -rf ./data/ ./goapp/data ./frontend-next/.next;;\
	* ) echo "Aborting";;\
	esac;